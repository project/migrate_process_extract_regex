<?php

namespace Drupal\migrate_process_extract_regex\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'MigrateProcessExtractRegex' migrate process plugin.
 *
 * Example:
 *
 * @code
 * process:
 *   'body/value':
 *     -
 *       plugin: migrate_process_html
 *       enablejs: true // optional defaults to true
 *       source: link
 *     -
 *       plugin: dom
 *       method: import
 *     -
 *       plugin: dom_select
 *       selector: //meta[@property="og:image"]/@content
 *     -
 *       plugin: skip_on_empty
 *       method: row
 *       message: 'Field image is missing'
 *     -
 *       plugin: extract
 *       index:
 *         - 0
 *     -
 *       plugin: skip_on_condition
 *       method: row
 *       condition:
 *         plugin: not:matches
 *       regex: /^(https?:\/\/)[\w\d]/i
 *       message: 'We only want a string if it starts with http(s)://[\d\w]'
 *     -
 *       plugin: migrate_process_extract_regex
 *       regex:  /^(.*)\?/ // will return the first match
 *     -
 *       plugin: file_remote_url
 *
 * @endcode
 *
 * @see \Drupal\migrate\Plugin\MigrateProcessInterface
 *
 * @MigrateProcessPlugin(
 *  id = "migrate_process_extract_regex"
 * )
 */
class MigrateProcessExtractRegex extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Logger Class object.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {

    $needle = FALSE;
    $haystack = $value;
    $configuration = $this->configuration;

    // Check for js enabled config setting and override if necessary.
    if (isset($configuration["regex"])) {
      $needle = $configuration["regex"];
      try {
        $output = $this->matchFirstRegExpr($needle, $haystack);
        return $output;
      }
      catch (\Throwable $e) {
        $error = $e->getMessage();
        $code = $e->getCode();
        $error_url_code_message = "Failed to parse $haystack, $needle. $error, $code";
        $this->logger->notice($error_url_code_message);
      }
    }
    else {
      return $haystack;
    }
  }

  /**
   * Returns first match for regular expression.
   *
   * @param string $needle
   *   Reg expression.
   * @param string $haystack
   *   Source that we use to parse reg expression.
   *
   * @return string
   *   Subpattern match or original haystack.
   */
  protected function matchFirstRegExpr($needle, $haystack) {
    if (is_string($haystack)) {
      preg_match($needle, $haystack, $match);
      // https://www.php.net/manual/en/function.preg-match.php
      // If matches is provided, then it is filled with the results of search.
      // $matches[0] will contain the text that matched the full pattern,
      // $matches[1] will have the text that matched the first captured
      // parenthesized subpattern, and so on.
      if (isset($match) && isset($match[0])) {
        return $match[1];
      }
      else {
        return $haystack;
      }
    }
    else {
      throw new MigrateException('When using the regex matches condition, the source must be a string.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.migrate_process_html')
    );
  }

}
